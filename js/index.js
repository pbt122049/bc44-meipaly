$(document).ready(function () {
  $(".c-carousel__list").slick({
    autoplay: true,
    arrows: false,
    speed: 1000,
    slidesToShow: 5,
    slidesToScroll: 1,
    dots: true,
  });
  $(".c-number__couter").countUp();
  $(window).scroll(function () {
    if ($(this).scrollTop()) {
      $(".c-backtotop").fadeIn();
    } else {
      $(".c-backtotop").fadeOut();
    }
  });
  $(".c-backtotop").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
  });
});
